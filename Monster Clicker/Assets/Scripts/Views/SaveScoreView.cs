﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


namespace MonsterClicker.Views
{
    public class SaveScoreView : MonoBehaviour
    {

        #region Fields

        [SerializeField]
        private Text _score;
        [SerializeField]
        private Text _inputName;
        [SerializeField]
        private Button _saveButton;

        #endregion


        #region Properties

        public string Name => _inputName.text;

        #endregion


        #region Methods

        public void DisplayScore(int score)
        {
            _score.text = score.ToString();
        }

        public void Initialize(UnityAction saveAction)
        {
            _saveButton.onClick.AddListener(saveAction);
        }

        #endregion


        #region UnityMethods

        private void OnDestroy()
        {
            _saveButton.onClick.RemoveAllListeners();
        }

        #endregion

    }
}
