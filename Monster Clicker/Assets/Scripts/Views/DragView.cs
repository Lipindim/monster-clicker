﻿using MonsterClicker.Tools;
using UnityEngine;
using UnityEngine.EventSystems;


namespace MonsterClicker.Views
{
    public class DragView : MonoBehaviour, IBeginDragHandler, IDragHandler
    {
        private GameEventSystem _gameEventSystem;

        public void Initialize(GameEventSystem gameEventSystem)
        {
            _gameEventSystem = gameEventSystem;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _gameEventSystem.SendDragBeginEvent(eventData.delta);
        }

        public void OnDrag(PointerEventData eventData)
        {
        }
    }
}
