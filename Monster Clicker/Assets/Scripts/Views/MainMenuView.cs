﻿using MonsterClicker.Enums;
using MonsterClicker.Tools;
using UnityEngine;
using UnityEngine.UI;


namespace MonsterClicker.Views
{
    public class MainMenuView : UiObjectView
    {

        #region Fields

        [SerializeField]
        private Button _startGameButton;
        [SerializeField]
        private Button _recordsButton;
        [SerializeField]
        private Button _creditsButton;
        [SerializeField]
        private Button _exitGameButton;

        #endregion


        #region Methods

        public override void Initialize(GameEventSystem gameEventSystem)
        {
            base.Initialize(gameEventSystem);
            _startGameButton.onClick.AddListener(StartGame);
            _recordsButton.onClick.AddListener(ShowRecords);
            _creditsButton.onClick.AddListener(ShowCredits);
            _exitGameButton.onClick.AddListener(ExitGame);
        }

        private void ExitGame()
        {
            _gameEventSystem.SendExitGameRequestedEvent();
        }

        private void ShowCredits()
        {
            _gameEventSystem.SendShowMenuRequestedEvent(MenuTabEnum.Credits);
        }

        private void ShowRecords()
        {
            _gameEventSystem.SendShowMenuRequestedEvent(MenuTabEnum.Records);
        }

        private void StartGame()
        {
            _gameEventSystem.SendStartGameRequestedEvent();
        }

        #endregion


        #region UnityMethods

        private void OnDestroy()
        {
            _startGameButton.onClick.RemoveAllListeners();
            _recordsButton.onClick.RemoveAllListeners();
            _creditsButton.onClick.RemoveAllListeners();
            _exitGameButton.onClick.RemoveAllListeners();
        }

        #endregion

    }
}
