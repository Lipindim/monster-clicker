﻿using MonsterClicker.Tools;
using UnityEngine;


namespace MonsterClicker.Views
{
    public class UiObjectView : MonoBehaviour
    {
        protected GameEventSystem _gameEventSystem;

        public virtual void Initialize(GameEventSystem gameEventSystem)
        {
            _gameEventSystem = gameEventSystem;
        }
    }
}
