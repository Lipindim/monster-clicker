﻿using UnityEngine;
using UnityEngine.UI;


namespace MonsterClicker.Views
{
    public class ScoreView : UiObjectView
    {
        [SerializeField]
        private Text _score;

        public void SetScore(int score)
        {
            _score.text = score.ToString();
        }
    }
}
