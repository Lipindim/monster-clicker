﻿using UnityEngine;
using UnityEngine.UI;


namespace MonsterClicker.Views
{
    public class MonsterCountView : UiObjectView
    {
        [SerializeField]
        private Text _monsterCountText;

        public void SetMonsterCount(int monsterCount)
        {
            _monsterCountText.text = monsterCount.ToString();
        }
    }
}
