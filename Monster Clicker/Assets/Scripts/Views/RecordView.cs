﻿using UnityEngine;
using UnityEngine.UI;


namespace MonsterClicker.Views
{
    public class RecordView : MonoBehaviour
    {
        [SerializeField]
        private Text _number;
        [SerializeField]
        private Text _nickname;
        [SerializeField]
        private Text _score;

        public void SetRecord(int number, string nickname, int score)
        {
            _number.text = number.ToString();
            _nickname.text = nickname;
            _score.text = score.ToString();
        }
    }
}
