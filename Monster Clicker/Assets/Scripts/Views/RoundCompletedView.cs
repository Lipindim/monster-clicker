﻿using MonsterClicker.Tools;
using UnityEngine;
using UnityEngine.UI;


namespace MonsterClicker.Views
{
    public class RoundCompletedView : UiObjectView
    {
        [SerializeField]
        private Button _nextRoundButton;

        public override void Initialize(GameEventSystem gameEventSystem)
        {
            base.Initialize(gameEventSystem);
            _nextRoundButton.onClick.AddListener(StartNextRound);
        }

        private void StartNextRound()
        {
            _gameEventSystem.SendNextRoundRequestedEvent();
        }

        private void OnDestroy()
        {
            _nextRoundButton.onClick.RemoveAllListeners();
        }
    }
}
