﻿using MonsterClicker.Enums;
using MonsterClicker.Tools;
using UnityEngine;
using UnityEngine.UI;


namespace MonsterClicker.Views
{
    public class RestartView : UiObjectView
    {

        #region Fields

        [SerializeField]
        private Button _restartButton;
        [SerializeField]
        private Button _mainMenuButton;
        [SerializeField]
        private Button _exitGameButton;

        #endregion


        #region Methods

        public override void Initialize(GameEventSystem gameEventSystem)
        {
            base.Initialize(gameEventSystem);
            _restartButton.onClick.AddListener(Restart);
            _mainMenuButton.onClick.AddListener(ShowMainMenu);
            _exitGameButton.onClick.AddListener(ExitGame);
        }

        private void ExitGame()
        {
            _gameEventSystem.SendExitGameRequestedEvent();
        }

        private void ShowMainMenu()
        {
            _gameEventSystem.SendShowMenuRequestedEvent(MenuTabEnum.Main);
        }

        private void Restart()
        {
            _gameEventSystem.SendRestartEvent();
        }

        #endregion


        #region UnityMethods

        private void OnDestroy()
        {
            _restartButton.onClick.RemoveAllListeners();
            _mainMenuButton.onClick.RemoveAllListeners();
            _exitGameButton.onClick.RemoveAllListeners();
        }

        #endregion

    }
}
