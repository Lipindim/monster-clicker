﻿using MonsterClicker.Enums;
using MonsterClicker.Tools;
using UnityEngine;
using UnityEngine.UI;


namespace MonsterClicker.Views
{
    public class ExtraMenuTabView : UiObjectView
    {
        [SerializeField]
        private Button _moveToBackButton;

        public override void Initialize(GameEventSystem gameEventSystem)
        {
            base.Initialize(gameEventSystem);
            _moveToBackButton.onClick.AddListener(ShowMainMenu);
        }

        private void ShowMainMenu()
        {
            _gameEventSystem.SendShowMenuRequestedEvent(MenuTabEnum.Main);
        }

        private void OnDestroy()
        {
            _moveToBackButton.onClick.RemoveAllListeners();
        }
    }
}
