﻿using MonsterClicker.Interfaces;
using MonsterClicker.Tools;
using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;


namespace MonsterClicker.Views
{
    public class MonsterView : MonoBehaviour, INavigation, IPointerClickHandler
    {

        #region Fields

        private NavMeshAgent _navMeshAgent;
        private GameEventSystem _gameEventSystem;
        private Guid _id;

        #endregion


        #region Properties

        public Vector3 CurrentPosition => transform.position;
        public Vector3 DestinationPosition { get; private set; }

        #endregion


        #region UnityMethods

        private void Awake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        #endregion


        #region Methods

        public void Initialize(GameEventSystem gameEventSystem, Guid id)
        {
            _gameEventSystem = gameEventSystem;
            _id = id;
        }

        public void SetPosition(Vector3 positoin)
        {
            transform.position = positoin;
            DestinationPosition = positoin;
        }

        #endregion


        #region INavigation

        public void SetDestination(Vector3 destionation)
        {
            if (_navMeshAgent != null)
            {
                DestinationPosition = destionation;
                _navMeshAgent.SetDestination(destionation);
            }
        }

        #endregion


        #region IPointerClickHandler

        public void OnPointerClick(PointerEventData eventData)
        {
            _gameEventSystem.SendMonsterClickEvent(_id);
        }

        public void SetSpeed(float speed)
        {
            if (_navMeshAgent != null)
                _navMeshAgent.speed = speed;
        }

        #endregion

    }
}
