﻿using MonsterClicker.Enums;
using System;
using UnityEngine;


namespace MonsterClicker.Views
{
    public class DirectionIndicatorView : UiObjectView
    {

        #region Fields

        [SerializeField]
        private GameObject _rightArrow;
        [SerializeField]
        private GameObject _leftArrow;
        [SerializeField]
        private GameObject _bottomArrow;
        [SerializeField]
        private GameObject _bottomRightArrow;
        [SerializeField]
        private GameObject _bottomLeftArrow;

        #endregion


        #region Methods

        public void ShowArrow(ArrowEnum arrowEnum)
        {
            GameObject arrow = GetArrowObject(arrowEnum);
            arrow.SetActive(true);
        }

        public void HideArrow(ArrowEnum arrowEnum)
        {
            GameObject arrow = GetArrowObject(arrowEnum);
            arrow.SetActive(false);
        }

        private GameObject GetArrowObject(ArrowEnum arrowEnum)
        {
            switch (arrowEnum)
            {
                case ArrowEnum.Right:
                    return _rightArrow;
                case ArrowEnum.Left:
                    return _leftArrow;
                case ArrowEnum.Bottom:
                    return _bottomArrow;
                case ArrowEnum.BottomLeft:
                    return _bottomLeftArrow;
                case ArrowEnum.BottomRight:
                    return _bottomRightArrow;
                default:
                    throw new ArgumentException($"Unkonwn arrow type: {arrowEnum}");
            }
        }

        #endregion

    }
}
