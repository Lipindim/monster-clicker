﻿using MonsterClicker.Enums;
using MonsterClicker.Models;
using MonsterClicker.Tools;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


namespace MonsterClicker.Views
{
    public class RecordsView : UiObjectView
    {

        #region Fields

        [SerializeField]
        private RecordView[] _recordViews;
        [SerializeField]
        private Button _backButton;

        #endregion


        #region Methods

        public void ShowRecords(List<RecordModel> recordModels)
        {
            RecordModel[] sortedRecords = recordModels.OrderByDescending(x => x.Score).ToArray();
            for (int i = 0; i < _recordViews.Length && i < sortedRecords.Length; i++)
                _recordViews[i].SetRecord(i + 1, sortedRecords[i].Nickname, sortedRecords[i].Score);
        }

        public override void Initialize(GameEventSystem gameEventSystem)
        {
            base.Initialize(gameEventSystem);
            _backButton.onClick.AddListener(ReturnToMainMenu);
        }

        private void ReturnToMainMenu()
        {
            _gameEventSystem.SendShowMenuRequestedEvent(MenuTabEnum.Main);
        }

        #endregion


        #region UnityMethods

        private void OnDestroy()
        {
            _backButton.onClick.RemoveAllListeners();
        }

        #endregion

    }
}
