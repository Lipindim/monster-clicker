﻿using MonsterClicker.Enums;
using MonsterClicker.Tools;
using UnityEngine;
using UnityEngine.UI;


namespace MonsterClicker.Views
{
    public class GameCompletedView : UiObjectView
    {

        #region Fields

        [SerializeField]
        private Button _newGameButton;
        [SerializeField]
        private Button _mainMenuButton;
        [SerializeField]
        private Button _exitGameButton;

        #endregion


        #region Methods

        public override void Initialize(GameEventSystem gameEventSystem)
        {
            base.Initialize(gameEventSystem);
            _newGameButton.onClick.AddListener(NewGame);
            _mainMenuButton.onClick.AddListener(ShowMainMenu);
            _exitGameButton.onClick.AddListener(ExitGame);
        }

        private void ExitGame()
        {
            _gameEventSystem.SendExitGameRequestedEvent();
        }

        private void ShowMainMenu()
        {
            _gameEventSystem.SendShowMenuRequestedEvent(MenuTabEnum.Main);
        }

        private void NewGame()
        {
            _gameEventSystem.SendStartGameRequestedEvent();
        }

        #endregion


        #region UnityMethods

        private void OnDestroy()
        {
            _newGameButton.onClick.RemoveAllListeners();
            _mainMenuButton.onClick.RemoveAllListeners();
            _exitGameButton.onClick.RemoveAllListeners();
        }

        #endregion

    }
}
