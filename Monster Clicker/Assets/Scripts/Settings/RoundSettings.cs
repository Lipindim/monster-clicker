using UnityEngine;


namespace MonsterClicker.Settings
{
    [CreateAssetMenu(fileName = "RoundSettings", menuName = "Data/Round Settings")]
    public class RoundSettings : ScriptableObject
    {
        public float Duration;
        public MonsterCount[] Enemies;
        public GameObject TilePrefab;
        public int fieldWidth;
        public int fieldHeight;
        public float monstersSpeed;
    }
}
