﻿using System;


namespace MonsterClicker.Settings
{
    [Serializable]
    public class MonsterCount
    {
        public MonsterSettings EnemySettings;
        public int Count;
    }
}
