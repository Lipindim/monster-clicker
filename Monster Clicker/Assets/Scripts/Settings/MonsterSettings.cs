using UnityEngine;

namespace MonsterClicker.Settings
{
    [CreateAssetMenu(fileName = "MonsterSettings", menuName = "Data/Monster Settings")]
    public class MonsterSettings : ScriptableObject
    {
        public GameObject Prefab;
        public int Health;
        public int Score;
    }
}
