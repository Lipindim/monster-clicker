﻿using UnityEngine;


namespace MonsterClicker.Settings
{
    [CreateAssetMenu(fileName = "GameSettings", menuName = "Data/Game Settings")]
    public class GameSettings : ScriptableObject
    {
        public RoundSettings[] Rounds;
        public int GameOverMonsterCount;
    }
}
