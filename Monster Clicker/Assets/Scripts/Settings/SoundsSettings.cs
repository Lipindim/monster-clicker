﻿using UnityEngine;


namespace MonsterClicker.Settings
{
    [CreateAssetMenu(fileName = "SoundSettings", menuName = "Data/Sound settings")]
    public class SoundsSettings : ScriptableObject
    {
        public AudioClip MonsterSpawnSound;
        public AudioClip MonsterClickSound;
        public AudioClip MonsterDeadSound;
        public AudioClip GameOverSound;
        public AudioClip RoundCompletedSound;
        public AudioClip GameCompletedSound;
    }
}
