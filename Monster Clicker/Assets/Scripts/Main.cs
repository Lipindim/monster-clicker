﻿using MonsterClicker.Controllers;
using UnityEngine;


namespace MonsterClicker
{
    public class Main : MonoBehaviour
    {

        #region Fields

        [SerializeField]
        private Transform _placeForUi;
        [SerializeField]
        private Transform _cameraPoint;

        private MainController _mainController;

        #endregion


        #region UnityMethods

        private void Awake()
        {
            _mainController = new MainController(_placeForUi, _cameraPoint);
        }

        private void Update()
        {
            _mainController.Update(Time.deltaTime);
        }

        private void OnDestroy()
        {
            _mainController.Dispose();
        }

        #endregion

    }
}
