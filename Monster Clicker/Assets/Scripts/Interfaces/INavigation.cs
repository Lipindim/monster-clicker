﻿using UnityEngine;


namespace MonsterClicker.Interfaces
{
    public interface INavigation
    {
        Vector3 CurrentPosition { get; }
        Vector3 DestinationPosition { get; }
        void SetDestination(Vector3 destination);
        void SetSpeed(float speed);
    }
}
