﻿namespace MonsterClicker.Interfaces
{
    public interface IUpdatable
    {
        public void Update(float deltaTime);
    }
}
