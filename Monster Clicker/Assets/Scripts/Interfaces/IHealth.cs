﻿namespace MonsterClicker.Interfaces
{
    public interface IHealth
    {
        int MaxHealth { get; }
        int CurrentHealth { get; }
        void TakeDamage(int damage);
    }
}
