﻿using MonsterClicker.Views;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace MonsterClicker.Tools
{
    public class FieldGenerator : IDisposable
    {

        #region Fields

        private readonly List<GameObject> _tiles;
        private readonly GameEventSystem _gameEventSystem;

        #endregion


        #region ClassLifeCycles

        public FieldGenerator(GameEventSystem gameEventSystem)
        {
            _tiles = new List<GameObject>();
            _gameEventSystem = gameEventSystem;
        }

        #endregion


        #region Methods

        public void GenerateField(int width, int height, GameObject tilePrefab)
        {
            ClearField();
            GenerateTiles(width, height, tilePrefab);
        }

        private void GenerateTiles(int width, int height, GameObject tilePrefab)
        {
            for (int indexX = 0; indexX < width; indexX++)
            {
                for (int indexZ = 0; indexZ < height; indexZ++)
                {
                    GameObject newTile = GameObject.Instantiate(tilePrefab);
                    DragView dragView = newTile.GetComponent<DragView>();
                    if (dragView == null)
                        dragView = newTile.AddComponent<DragView>();
                    dragView.Initialize(_gameEventSystem);
                    newTile.transform.position = new Vector3(indexX, tilePrefab.transform.position.y, indexZ);
                    _tiles.Add(newTile);
                }
            }
        }

        private void ClearField()
        {
            foreach (var tile in _tiles)
                GameObject.Destroy(tile);
            _tiles.Clear();
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            ClearField();
        }

        #endregion

    }
}
