﻿using MonsterClicker.Enums;
using MonsterClicker.Models;
using MonsterClicker.Settings;
using System;
using UnityEngine;


namespace MonsterClicker.Tools
{
    public class GameEventSystem
    {
        public event Action<MonsterModel> OnMonsterSpawned;
        public event Action<Guid> OnMonsterClick;
        public event Action<Guid> OnMonsterDead;
        public event Action<Guid> OnMonsterHit;
        public event Action<MenuTabEnum> OnShowMenuRequested;
        public event Action<Vector2> OnDragBegin;
        public event Action<RoundSettings> OnRoundStarted;
        public event Action<ArrowEnum> OnShowArrowRequested;

        public event Action OnGameOver;
        public event Action OnRestart;
        public event Action OnRoundCompleted;
        public event Action OnGameCompleted;
        public event Action OnNextRoundRequested;
        public event Action OnExitGameRequested;
        public event Action OnStartGameRequested;
        

        public void SendMonsterSpawnedEvent(MonsterModel monster)
        {
            OnMonsterSpawned?.Invoke(monster);
        }

        public void SendMonsterClickEvent(Guid id)
        {
            OnMonsterClick?.Invoke(id);
        }

        public void SendMonsterDeadEvent(Guid id)
        {
            OnMonsterDead?.Invoke(id);
        }

        public void SendMonsterHitEvent(Guid id)
        {
            OnMonsterHit?.Invoke(id);
        }

        public void SendRestartEvent()
        {
            OnRestart?.Invoke();
        }

        public void SendGameOverEvent()
        {
            OnGameOver?.Invoke();
        }

        public void SendRoundCompletedEvent()
        {
            OnRoundCompleted?.Invoke();
        }

        public void SendGameCompletedEvent()
        {
            OnGameCompleted?.Invoke();
        }

        public void SendNextRoundRequestedEvent()
        {
            OnNextRoundRequested?.Invoke();
        }

        public void SendStartGameRequestedEvent()
        {
            OnStartGameRequested?.Invoke();
        }

        public void SendShowMenuRequestedEvent(MenuTabEnum requestedMenu)
        {
            OnShowMenuRequested?.Invoke(requestedMenu);
        }

        public void SendExitGameRequestedEvent()
        {
            OnExitGameRequested?.Invoke();
        }

        public void SendRoundStartedEvent(RoundSettings roundSettings)
        {
            OnRoundStarted?.Invoke(roundSettings);
        }

        public void SendDragBeginEvent(Vector2 delta)
        {
            OnDragBegin?.Invoke(delta);
        }

        public void SendShowArrowRequestedEvent(ArrowEnum arrowEnum)
        {
            OnShowArrowRequested?.Invoke(arrowEnum);
        }
    }
}
