﻿using System;

namespace MonsterClicker.Tools
{
    public class UnityTimer
    {

        #region Fields

        private float _timeUntilNextSpawn;
        private float _interval;

        #endregion


        #region Events

        public event Action OnTimerTick;

        #endregion


        #region ClassLifeCycles

        public UnityTimer(float interval, float startToFirst)
        {
            _interval = interval;
            _timeUntilNextSpawn = startToFirst;
        }

        #endregion


        #region Methods

        public void Tick(float deltaTime)
        {
            _timeUntilNextSpawn -= deltaTime;
            if (_timeUntilNextSpawn <= 0)
            {
                OnTimerTick?.Invoke();
                Reset();
            }
        }

        private void Reset()
        {
            _timeUntilNextSpawn = _interval;
        }

        #endregion

    }
}
