﻿using MonsterClicker.Models;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace MonsterClicker.Tools
{
    public class RecordsStore
    {

        #region Constants

        private const string RECORDS_KEY = "recordKey";

        #endregion


        #region Fields

        private static List<RecordModel> _defaultRecords = new List<RecordModel>
            {
                new RecordModel("Devil", 500),
                new RecordModel("Panda", 480),
                new RecordModel("Chak", 470),
                new RecordModel("Ivan", 440),
                new RecordModel("Fly", 400)
            };

        #endregion


        #region Methods

        public bool IsRecord(int score)
        {
            List<RecordModel> records = GetRecords();
            RecordModel lowestRecord = GetLowestRecord(records);
            return lowestRecord.Score < score;
        }

        public List<RecordModel> GetRecords()
        {
            string json =  PlayerPrefs.GetString(RECORDS_KEY);
            List<RecordModel> records;
            if (!string.IsNullOrWhiteSpace(json))
            {
                var recordCollection = JsonUtility.FromJson<RecordCollection>(json);
                records = recordCollection.Records;
            }
            else records = _defaultRecords;
            return records;
        }

        public void SaveRecord(int score, string nickname)
        {
            List<RecordModel> records = GetRecords();
            RecordModel lowResult = GetLowestRecord(records);

            records.Remove(lowResult);
            records.Add(new RecordModel(nickname, score));
            var recordCollection = new RecordCollection()
            {
                Records = records
            };

            string json = JsonUtility.ToJson(recordCollection);

            PlayerPrefs.SetString(RECORDS_KEY, json);
            PlayerPrefs.Save();
        }

        private static RecordModel GetLowestRecord(List<RecordModel> records)
        {
            return records
                .OrderBy(x => x.Score)
                .First();
        }

        #endregion

    }
}
