﻿using MonsterClicker.Interfaces;
using MonsterClicker.Models;
using MonsterClicker.Settings;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace MonsterClicker.Tools
{
    public class SpawnPositoinCalculator : IDisposable
    {

        #region Constants

        private const float SQR_MIN_DISTANSE_BETWEEN_MONSTERS = 0.25f;

        #endregion


        #region Fields

        private readonly GameEventSystem _gameEventSystem;
        private readonly Dictionary<Guid, INavigation> _monsterNavigations;
        private readonly System.Random _random;

        private int _fieldWidth;
        private int _fieldHeight;

        #endregion


        #region ClassLifeCycles

        public SpawnPositoinCalculator(GameEventSystem gameEventSystem)
        {
            _gameEventSystem = gameEventSystem;
            _monsterNavigations = new Dictionary<Guid, INavigation>();
            _random = new System.Random();
            _gameEventSystem.OnMonsterSpawned += AddNewMonster;
            _gameEventSystem.OnMonsterDead += RemoveMonster;
            _gameEventSystem.OnRoundStarted += SetFieldSize;
        }

        #endregion


        #region Methods

        public Vector3 GetSpawnPosition(float y)
        {
            Vector3 newPosition;

            do
            {
                float randomX = _random.Next(_fieldWidth);
                float randomZ = _random.Next(_fieldHeight);
                newPosition = new Vector3(randomX, y, randomZ);
            } while (IsMonsterNearby(newPosition));

            return newPosition;
        }

        private bool IsMonsterNearby(Vector3 spawnPosition)
        {
            foreach (var monsterNavigation in _monsterNavigations.Values)
            {
                Vector3 vectorBetweenMonsters = monsterNavigation.CurrentPosition - spawnPosition;
                if (vectorBetweenMonsters.sqrMagnitude < SQR_MIN_DISTANSE_BETWEEN_MONSTERS)
                    return true;
            }

            return false;
        }

        private void SetFieldSize(RoundSettings roundSettings)
        {
            _fieldHeight = roundSettings.fieldHeight;
            _fieldWidth = roundSettings.fieldWidth;
        }

        private void RemoveMonster(Guid id)
        {
            if (_monsterNavigations.ContainsKey(id))
                _monsterNavigations.Remove(id);
        }

        private void AddNewMonster(MonsterModel monster)
        {
            _monsterNavigations.Add(monster.Id, monster);
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            _gameEventSystem.OnMonsterSpawned -= AddNewMonster;
            _gameEventSystem.OnMonsterDead -= RemoveMonster;
            _gameEventSystem.OnRoundStarted -= SetFieldSize;
        }

        #endregion

    }
}
