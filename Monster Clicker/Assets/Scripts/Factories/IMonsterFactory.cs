﻿using MonsterClicker.Models;
using MonsterClicker.Settings;


namespace MonsterClicker.Factories
{
    public interface IMonsterFactory
    {
        MonsterModel CreateMonster(MonsterSettings monsterSettings, float monsterSpeed);
    }
}
