﻿using MonsterClicker.Models;
using MonsterClicker.Pool;
using MonsterClicker.Settings;
using MonsterClicker.Tools;
using MonsterClicker.Views;
using System;
using UnityEngine;


namespace MonsterClicker.Factories
{
    public class MonsterFactory : IMonsterFactory, IDisposable
    {

        #region Methods

        private readonly PoolServices _poolServices;
        private readonly GameEventSystem _gameEventSystem;
        private readonly SpawnPositoinCalculator _spawnPositionCalculator;

        #endregion


        #region ClassLifeCycles

        public MonsterFactory(PoolServices poolServicesp, GameEventSystem gameEventSystem)
        {
            _poolServices = poolServicesp;
            _gameEventSystem = gameEventSystem;
            _spawnPositionCalculator = new SpawnPositoinCalculator(_gameEventSystem);
        }

        #endregion


        #region Methods

        public MonsterModel CreateMonster(MonsterSettings monsterSettings, float monsterSpeed)
        {
            GameObject monsterObject = _poolServices.Create(monsterSettings.Prefab);
            var monsterView = monsterObject.GetComponent<MonsterView>();
            if (monsterView == null)
                monsterView = monsterObject.AddComponent<MonsterView>();
            Guid id = Guid.NewGuid();
            monsterView.Initialize(_gameEventSystem, id);
            Vector3 spawnPosition = _spawnPositionCalculator.GetSpawnPosition(monsterSettings.Prefab.transform.position.y);
            monsterView.SetPosition(spawnPosition);
            var monsterHealth = new MonsterHealth(monsterSettings.Health);
            var monsterModel = new MonsterModel(id, monsterObject, monsterHealth, monsterView, monsterSettings.Score);
            monsterModel.SetSpeed(monsterSpeed);
            return monsterModel;
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            _spawnPositionCalculator.Dispose();
        }

        #endregion

    }
}
