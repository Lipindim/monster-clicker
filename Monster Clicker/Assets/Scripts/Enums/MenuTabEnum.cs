﻿namespace MonsterClicker.Enums
{
    public enum MenuTabEnum
    {
        None = 0,
        Main = 1,
        Records = 2,
        Credits = 3
    }
}
