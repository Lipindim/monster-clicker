﻿namespace MonsterClicker.Enums
{
    public enum ArrowEnum
    {
        None = 0,
        Right = 1,
        Left = 2,
        Bottom = 3,
        BottomLeft = 4,
        BottomRight = 5
    }
}
