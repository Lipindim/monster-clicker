﻿using MonsterClicker.Interfaces;


namespace MonsterClicker.Models
{
    public class MonsterHealth : IHealth
    {
        public int CurrentHealth { get; private set; }

        public int MaxHealth { get; }

        public MonsterHealth(int health)
        {
            MaxHealth = health;
            CurrentHealth = MaxHealth;
        }

        public void TakeDamage(int damage)
        {
            CurrentHealth -= damage;
        }
    }
}
