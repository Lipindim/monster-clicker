﻿using System;
using System.Collections.Generic;

namespace MonsterClicker.Models
{
    [Serializable]
    public class RecordCollection
    {
        public List<RecordModel> Records;
    }
}
