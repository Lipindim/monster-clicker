﻿using MonsterClicker.Interfaces;
using System;
using UnityEngine;


namespace MonsterClicker.Models
{
    public class MonsterModel : IHealth, INavigation
    {

        #region Fields

        private readonly IHealth _health;
        private readonly INavigation _navigation;

        #endregion


        #region Properties

        public Guid Id { get; }
        public GameObject GameObject { get; }
        public int Score { get; }
        public int MaxHealth => _health.MaxHealth;
        public int CurrentHealth => _health.CurrentHealth;
        public Vector3 CurrentPosition => _navigation.CurrentPosition;
        public Vector3 DestinationPosition => _navigation.DestinationPosition;

        #endregion


        #region ClassLifeCycles

        public MonsterModel(Guid id, GameObject gameObject, IHealth health, INavigation navigation, int score)
        {
            Id = id;
            GameObject = gameObject;
            _health = health;
            _navigation = navigation;
            Score = score;
        }

        #endregion


        #region INavigation

        public void SetDestination(Vector3 destination)
        {
            _navigation.SetDestination(destination);
        }

        public void SetSpeed(float speed)
        {
            _navigation.SetSpeed(speed);
        }

        #endregion


        #region IHealth

        public void TakeDamage(int damage)
        {
            _health.TakeDamage(damage);
        }

        #endregion

    }
}
