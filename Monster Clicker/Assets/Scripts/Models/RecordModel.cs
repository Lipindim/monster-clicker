﻿using System;

namespace MonsterClicker.Models
{
    [Serializable]
    public class RecordModel
    {
        public string Nickname;
        public int Score;

        public RecordModel()
        {

        }

        public RecordModel(string nickname, int score)
        {
            Nickname = nickname;
            Score = score;
        }
    }
}
