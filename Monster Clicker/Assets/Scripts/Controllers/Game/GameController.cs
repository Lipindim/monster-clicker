﻿using MonsterClicker.Interfaces;
using MonsterClicker.Settings;
using MonsterClicker.Tools;
using System;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class GameController : IUpdatable, IDisposable
    {

        #region Constants

        private const string GAME_SETTINGS_PATH = "Configs/GameSettings";

        #endregion


        #region Fields

        private readonly RoundsController _roundsController;
        private readonly CameraController _cameraController;
        private readonly GameOverController _gameOverController;
        private readonly SoundController _soundController;
        private readonly GameEventSystem _gameEventSystem;

        #endregion


        #region ClassLifeCycles

        public GameController(GameEventSystem gameEventSystem, Transform cameraPoint)
        {
            GameSettings gameSettings = Resources.Load<GameSettings>(GAME_SETTINGS_PATH);

            _roundsController = new RoundsController(gameEventSystem, gameSettings.Rounds);
            _cameraController = new CameraController(cameraPoint, gameEventSystem);
            _gameOverController = new GameOverController(gameSettings.GameOverMonsterCount
                , gameEventSystem);
            _soundController = new SoundController(gameEventSystem);
            

            _gameEventSystem = gameEventSystem;
            _gameEventSystem.OnStartGameRequested += StartGame;
        }

        #endregion


        #region Methods

        private void StartGame()
        {
            _roundsController.Start();
        }

        #endregion


        #region IUpdatable

        public void Update(float deltaTime)
        {
            _roundsController.Update(deltaTime);
            _cameraController.Update(deltaTime);
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            _gameEventSystem.OnStartGameRequested -= StartGame;

            _cameraController.Dispose();
            _gameOverController.Dispose();
            _roundsController.Dispose();
            _soundController.Dispose();
        }

        #endregion

    }
}
