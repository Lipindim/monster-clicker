﻿using MonsterClicker.Models;
using MonsterClicker.Settings;
using MonsterClicker.Tools;
using System;


namespace MonsterClicker.Controllers
{
    public class GameOverController : IDisposable
    {

        #region Fields

        private GameEventSystem _gameEventSystem;

        private int _gameOverUnitCount;
        private int _currentMonsterCount;

        #endregion


        #region ClassLifeCycles

        public GameOverController(int gameOverUnitCount, GameEventSystem gameEventSystem)
        {
            _gameOverUnitCount = gameOverUnitCount;
            _gameEventSystem = gameEventSystem;
            _gameEventSystem.OnMonsterSpawned += IncreaseMonsterCount;
            _gameEventSystem.OnMonsterDead += DecreaseMonsterCount;
            _gameEventSystem.OnRoundStarted += ResetMonsterCount;
        }

        #endregion


        #region Methods

        private void ResetMonsterCount(RoundSettings roundSettings)
        {
            _currentMonsterCount = 0;
        }

        private void DecreaseMonsterCount(Guid id)
        {
            _currentMonsterCount--;
        }

        private void IncreaseMonsterCount(MonsterModel monsterModel)
        {
            _currentMonsterCount++;
            if (_currentMonsterCount >= _gameOverUnitCount)
                _gameEventSystem.SendGameOverEvent();
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            _gameEventSystem.OnMonsterSpawned -= IncreaseMonsterCount;
            _gameEventSystem.OnMonsterDead -= DecreaseMonsterCount;
            _gameEventSystem.OnRoundStarted -= ResetMonsterCount;
        }

        #endregion

    }
}
