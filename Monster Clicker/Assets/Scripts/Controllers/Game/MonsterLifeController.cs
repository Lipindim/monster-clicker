﻿using MonsterClicker.Models;
using MonsterClicker.Pool;
using MonsterClicker.Settings;
using MonsterClicker.Tools;
using System;
using System.Collections.Generic;


namespace MonsterClicker.Controllers
{
    public class MonsterLifeController : IDisposable
    {

        #region Fields

        private readonly Dictionary<Guid, MonsterModel> _activeMonsters;
        private readonly GameEventSystem _gameEventSystem;
        private readonly PoolServices _poolServices;

        #endregion


        #region ClassLifeCycles

        public MonsterLifeController(GameEventSystem gameEventSystem, PoolServices poolServices)
        {
            _activeMonsters = new Dictionary<Guid, MonsterModel>();
            _gameEventSystem = gameEventSystem;
            _poolServices = poolServices;

            _gameEventSystem.OnMonsterSpawned += GameEventSystemOnMonsterSpawned;
            _gameEventSystem.OnRoundStarted += ClearActiveMonsters;
            _gameEventSystem.OnMonsterClick += TakeDamage;
        }

        private void TakeDamage(Guid id)
        {
            if (_activeMonsters.TryGetValue(id, out MonsterModel monster))
            {
                monster.TakeDamage(1);
                if (monster.CurrentHealth <= 0)
                {
                    _poolServices.Destroy(monster.GameObject);
                    _gameEventSystem.SendMonsterDeadEvent(id);
                    _activeMonsters.Remove(id);
                }
                else
                {
                    _gameEventSystem.SendMonsterHitEvent(id);
                }
            }
        }

        #endregion


        #region Methods

        private void ClearActiveMonsters(RoundSettings roundSettings)
        {
            Clear();
        }


        private void GameEventSystemOnMonsterSpawned(MonsterModel monster)
        {
            _activeMonsters.Add(monster.Id, monster);
        }

        private void Clear()
        {
            foreach (MonsterModel monster in _activeMonsters.Values)
                _poolServices.Destroy(monster.GameObject);
            _activeMonsters.Clear();
        }

        #endregion


        #region IDispasable

        public void Dispose()
        {
            _gameEventSystem.OnMonsterSpawned -= GameEventSystemOnMonsterSpawned;
            _gameEventSystem.OnRoundStarted -= ClearActiveMonsters;
            _gameEventSystem.OnMonsterClick -= TakeDamage;

            Clear();
        }

        #endregion

    }
}
