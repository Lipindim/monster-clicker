﻿using MonsterClicker.Interfaces;
using MonsterClicker.Settings;
using MonsterClicker.Tools;
using System;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class CameraController : IDisposable, IUpdatable
    {

        #region Constants

        private const float DELTA = 0.05f;
        private const float OFFSET_X = -2.0f;
        private const float OFFSET_Z = -1.0f;

        #endregion


        #region Fields

        private readonly GameEventSystem _gameEventSystem;
        private readonly Transform _cameraPoint;
        private readonly HitsTheCameraController _hitsTheCameraController;

        private readonly Vector3 _startPosition;

        private Vector3 _targetPoint;
        private int _fieldWidth;
        private int _fieldHeight;

        #endregion


        #region ClassLifeCycles

        public CameraController(Transform cameraPoint, GameEventSystem gameEventSystem)
        {
            _startPosition = cameraPoint.position;
            _cameraPoint = cameraPoint;
            _gameEventSystem = gameEventSystem;
            _gameEventSystem.OnRoundStarted += PrepareCamera;
            _gameEventSystem.OnDragBegin += SetTargetPoint;

            Camera camera = cameraPoint.gameObject.GetComponentInChildren<Camera>();
            _hitsTheCameraController = new HitsTheCameraController(gameEventSystem, camera);
        }

        #endregion


        #region Methods

        private void SetTargetPoint(Vector2 delta)
        {
            if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y))
            {
                if (delta.x > 0)
                    _targetPoint = _cameraPoint.position + Vector3.left;
                else
                    _targetPoint = _cameraPoint.position + Vector3.right;
            }
            else
            {
                if (delta.y > 0)
                    _targetPoint = _cameraPoint.position + Vector3.back;
                else
                    _targetPoint = _cameraPoint.position + Vector3.forward;
            }
        }

        private void PrepareCamera(RoundSettings roundSettings)
        {
            _cameraPoint.transform.position = _startPosition;
            _targetPoint = _startPosition;
            _fieldWidth = roundSettings.fieldWidth;
            _fieldHeight = roundSettings.fieldHeight;
        }


        private void MoveCameraPoint()
        {
            if (Mathf.Abs(_cameraPoint.position.x - _targetPoint.x) >= DELTA)
            {
                float deltaWithDirection = Mathf.Sign(_cameraPoint.position.x - _targetPoint.x) * DELTA;

                if (WidthInCameraRange(deltaWithDirection))
                    _cameraPoint.position += new Vector3(-deltaWithDirection, 0, 0);
            }
            if (Mathf.Abs(_cameraPoint.position.z - _targetPoint.z) >= DELTA)
            {
                float deltaWithDirection = Mathf.Sign(_cameraPoint.position.z - _targetPoint.z) * DELTA;
                
                if (HeightInCameraRange(deltaWithDirection))
                    _cameraPoint.position += new Vector3(0, 0, -deltaWithDirection);
            }
        }

        private bool HeightInCameraRange(float deltaWithDirection)
        {
            return _cameraPoint.position.z - deltaWithDirection > _startPosition.z
                && _cameraPoint.position.z - deltaWithDirection < _startPosition.z + _fieldHeight + OFFSET_X;
        }

        private bool WidthInCameraRange(float deltaWithDirection)
        {
            return _cameraPoint.position.x - deltaWithDirection > _startPosition.x
                && _cameraPoint.position.x - deltaWithDirection < _startPosition.x + _fieldWidth + OFFSET_Z;
        }

        #endregion


        #region IUpdatable

        public void Update(float deltaTime)
        {
            MoveCameraPoint();
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            _gameEventSystem.OnRoundStarted -= PrepareCamera;
            _gameEventSystem.OnDragBegin -= SetTargetPoint;

            _hitsTheCameraController.Dispose();
        }

        #endregion

    }
}
