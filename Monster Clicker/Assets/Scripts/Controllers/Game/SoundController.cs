﻿using MonsterClicker.Models;
using MonsterClicker.Settings;
using MonsterClicker.Tools;
using System;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class SoundController : IDisposable
    {

        #region Constants

        private const string PREFAB_PATH = "Prefabs/AudioSource";
        private const string SOUND_SETTINGS_PATH = "Configs/SoundSettings";

        #endregion


        #region Fields

        private readonly GameEventSystem _gameEventSystem;
        private readonly AudioSource _audioSource;
        private readonly SoundsSettings _soundsSettings;

        #endregion


        #region ClassLifeCycles

        public SoundController(GameEventSystem gameEventSystem)
        {
            _gameEventSystem = gameEventSystem;
            _audioSource = LoadAudioSource();
            _soundsSettings = Resources.Load<SoundsSettings>(SOUND_SETTINGS_PATH);
            _gameEventSystem.OnMonsterSpawned += GameEventSystemOnMonsterSpawned;
            _gameEventSystem.OnMonsterDead += GgameEventSystemOnMonsterDead;
            _gameEventSystem.OnMonsterHit += GameEventSystemOnMonsterHit;
            _gameEventSystem.OnRoundCompleted += GameEventSystemOnRoundCompleted;
            _gameEventSystem.OnGameCompleted += GameEventSystemOnGameCompleted;
            _gameEventSystem.OnGameOver += GameEventSystemOnGameOver;
        }

        #endregion


        #region Methods

        private void GameEventSystemOnMonsterHit(Guid id)
        {
            _audioSource.PlayOneShot(_soundsSettings.MonsterClickSound);
        }

        private void GameEventSystemOnGameOver()
        {
            _audioSource.PlayOneShot(_soundsSettings.GameOverSound);
        }

        private void GameEventSystemOnGameCompleted()
        {
            _audioSource.PlayOneShot(_soundsSettings.GameCompletedSound);
        }

        private void GameEventSystemOnRoundCompleted()
        {
            _audioSource.PlayOneShot(_soundsSettings.RoundCompletedSound);
        }

        private void GgameEventSystemOnMonsterDead(Guid id)
        {
            _audioSource.PlayOneShot(_soundsSettings.MonsterDeadSound);
        }

        private void GameEventSystemOnMonsterSpawned(MonsterModel monster)
        {
            _audioSource.PlayOneShot(_soundsSettings.MonsterSpawnSound);
        }

        private AudioSource LoadAudioSource()
        {
            GameObject prefab = Resources.Load<GameObject>(PREFAB_PATH);
            var audioSourceObject = GameObject.Instantiate(prefab);
            return audioSourceObject.GetComponent<AudioSource>();
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            _gameEventSystem.OnMonsterSpawned -= GameEventSystemOnMonsterSpawned;
            _gameEventSystem.OnMonsterDead -= GgameEventSystemOnMonsterDead;
            _gameEventSystem.OnMonsterHit -= GameEventSystemOnMonsterHit;
            _gameEventSystem.OnRoundCompleted -= GameEventSystemOnRoundCompleted;
            _gameEventSystem.OnGameCompleted -= GameEventSystemOnGameCompleted;
            _gameEventSystem.OnGameOver -= GameEventSystemOnGameOver;
        }

        #endregion

    }
}
