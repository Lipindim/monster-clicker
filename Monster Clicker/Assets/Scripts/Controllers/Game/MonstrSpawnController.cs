﻿using MonsterClicker.Factories;
using MonsterClicker.Interfaces;
using MonsterClicker.Models;
using MonsterClicker.Pool;
using MonsterClicker.Settings;
using MonsterClicker.Tools;
using System;
using System.Collections.Generic;
using System.Linq;


namespace MonsterClicker.Controllers
{
    public class MonstrSpawnController : IUpdatable, IDisposable
    {

        #region Constants

        private const float START_DELAY = 1.0f;

        #endregion


        #region Fields

        private readonly System.Random _random;
        private readonly GameEventSystem _gameEventSystem;
        private readonly IMonsterFactory _monsterFactory;

        private Dictionary<MonsterSettings, int> _enemies;
        private UnityTimer _unityTimer;
        
        private bool _spawnCompleted;
        private float _monsterSpeed;

        #endregion


        #region ClassLifeCycles

        public MonstrSpawnController(PoolServices poolServices, GameEventSystem gameEventSystem)
        {
            _enemies = new Dictionary<MonsterSettings, int>();
            _random = new System.Random();
            _monsterFactory = new MonsterFactory(poolServices, gameEventSystem);

            _gameEventSystem = gameEventSystem;
            _gameEventSystem.OnGameOver += StopSpawn;
        }

        #endregion


        #region Methods

        private void StopSpawn()
        {
            _spawnCompleted = true;
        }

        public void SetSpawnSettings(RoundSettings roundSettings)
        {
            _enemies.Clear();

            foreach (var enemy in roundSettings.Enemies)
                _enemies.Add(enemy.EnemySettings, enemy.Count);

            _spawnCompleted = false;
            _monsterSpeed = roundSettings.monstersSpeed;

            float spawnInterval = roundSettings.Duration / _enemies.Sum(x => x.Value);
            UpdateSpawnTimer(spawnInterval);
        }

        private void UpdateSpawnTimer(float spawnInterval)
        {
            if (_unityTimer != null)
                _unityTimer.OnTimerTick -= UnityTimerOnTimerTick;
            _unityTimer = new UnityTimer(spawnInterval, START_DELAY);
            _unityTimer.OnTimerTick += UnityTimerOnTimerTick;
        }

        private void UnityTimerOnTimerTick()
        {
            if (!_spawnCompleted)
            {
                if (_enemies.Sum(x => x.Value) == 0)
                    _spawnCompleted = true;
                else
                    SpawnMonster();
            }
        }

        private void SpawnMonster()
        {
            MonsterSettings monsterSettings = ChoosMonster();
            MonsterModel monster = _monsterFactory.CreateMonster(monsterSettings, _monsterSpeed);
            _enemies[monsterSettings]--;
            _gameEventSystem.SendMonsterSpawnedEvent(monster);
        }

        private MonsterSettings ChoosMonster()
        {
            int totalEnemiesCount = _enemies.Sum(x => x.Value);
            int randomIndex = _random.Next(totalEnemiesCount);

            int currentCount = 0;

            foreach (var enemy in _enemies)
            {
                currentCount += enemy.Value;
                if (currentCount > randomIndex)
                    return enemy.Key;
            }

            throw new InvalidOperationException("Can't choos enemy.");
        }

        #endregion


        #region IUpdatable

        public void Update(float deltaTime)
        {
            if (_unityTimer != null)
                _unityTimer.Tick(deltaTime);
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            _gameEventSystem.OnGameOver -= StopSpawn;
        }

        #endregion

    }
}
