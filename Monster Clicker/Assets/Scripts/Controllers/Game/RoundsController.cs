﻿using MonsterClicker.Interfaces;
using MonsterClicker.Pool;
using MonsterClicker.Settings;
using MonsterClicker.Tools;
using System;
using System.Linq;


namespace MonsterClicker.Controllers
{
    public class RoundsController : IUpdatable, IDisposable
    {

        #region Fields

        private readonly RoundSettings[] _roundsSettings;
        private readonly MonstrSpawnController _enemySpawnController;
        private readonly FieldGenerator _fieldGenerator;
        private readonly MonsterLifeController _monsterLifeController;
        private readonly NavigationController _navigationController;
        private readonly PoolServices _poolServices;
        private readonly GameEventSystem _gameEventSystem;

        private int _currentRound;
        private int _remainedMonstersCount;

        #endregion


        #region ClassLifeCycles

        public RoundsController(GameEventSystem gameEventSystem, RoundSettings[] roundSettings)
        {
            _poolServices = new PoolServices();
            _enemySpawnController = new MonstrSpawnController(_poolServices, gameEventSystem);
            _fieldGenerator = new FieldGenerator(gameEventSystem);
            _monsterLifeController = new MonsterLifeController(gameEventSystem, _poolServices);
            _navigationController = new NavigationController(gameEventSystem);

            _gameEventSystem = gameEventSystem;
            _roundsSettings = roundSettings;
            
            _gameEventSystem.OnRestart += RestartCurrentRound;
            _gameEventSystem.OnMonsterDead += UpdateMonsterCount;
            _gameEventSystem.OnNextRoundRequested += StartNextRound;
        }

        #endregion


        #region Methods

        public void Start()
        {
            _currentRound = 0;
            StartRound(_roundsSettings[_currentRound]);
        }

        private void StartNextRound()
        {
            if (WasLastRound())
                _gameEventSystem.SendGameCompletedEvent();
            else
                StartRound(_roundsSettings[++_currentRound]);
        }

        private void UpdateMonsterCount(Guid id)
        {
            _remainedMonstersCount--;
            if (_remainedMonstersCount <= 0)
            {
                if (WasLastRound())
                    _gameEventSystem.SendGameCompletedEvent();
                else
                    _gameEventSystem.SendRoundCompletedEvent();
            }
        }

        private bool WasLastRound()
        {
            return _currentRound == _roundsSettings.Length - 1;
        }

        private void RestartCurrentRound()
        {
            StartRound(_roundsSettings[_currentRound]);
        }

        private void StartRound(RoundSettings roundSettings)
        {
            _remainedMonstersCount = roundSettings.Enemies.Sum(x => x.Count);
            _fieldGenerator.GenerateField(roundSettings.fieldWidth, roundSettings.fieldHeight, roundSettings.TilePrefab);
            _enemySpawnController.SetSpawnSettings(roundSettings);
            _gameEventSystem.SendRoundStartedEvent(roundSettings);
        }

        #endregion


        #region IUpdatable

        public void Update(float deltaTime)
        {
            _enemySpawnController.Update(deltaTime);
            _navigationController.Update(deltaTime);
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            _gameEventSystem.OnRestart -= RestartCurrentRound;
            _gameEventSystem.OnMonsterDead -= UpdateMonsterCount;
            _gameEventSystem.OnNextRoundRequested -= StartNextRound;
            _monsterLifeController.Dispose();
            _enemySpawnController.Dispose();
            _fieldGenerator.Dispose();
        }

        #endregion

    }
}
