﻿using MonsterClicker.Interfaces;
using MonsterClicker.Models;
using MonsterClicker.Tools;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class NavigationController : IUpdatable, IDisposable
    {

        #region Constants

        private const float DELTA = 0.1f;

        #endregion


        #region Fields

        private readonly GameEventSystem _gameEventSystem;
        private readonly Dictionary<Guid, INavigation> _monsterNavigations;
        private readonly System.Random _random;

        private int _fieldHeight;
        private int _fieldWidth;

        #endregion


        #region ClassLifeCycles

        public NavigationController(GameEventSystem gameEventSystem)
        {
            _monsterNavigations = new Dictionary<Guid, INavigation>();
            _random = new System.Random();

            _gameEventSystem = gameEventSystem;
            _gameEventSystem.OnMonsterSpawned += AddNewMonster;
            _gameEventSystem.OnRoundStarted += PrepareNewRound;
            _gameEventSystem.OnMonsterDead += RemoveMonster;
        }

        #endregion


        #region Methods

        private void RemoveMonster(Guid id)
        {
            if (_monsterNavigations.ContainsKey(id))
                _monsterNavigations.Remove(id);
        }

        private void PrepareNewRound(Settings.RoundSettings roundSettings)
        {
            _monsterNavigations.Clear();
            _fieldHeight = roundSettings.fieldHeight;
            _fieldWidth = roundSettings.fieldWidth;
        }

        private void AddNewMonster(MonsterModel monsterModel)
        {
            _monsterNavigations.Add(monsterModel.Id, monsterModel);
        }

        private bool IsMonsterReachdDestination(INavigation monsterNavigation)
        {
            Vector3 vectorToDestination = monsterNavigation.DestinationPosition - monsterNavigation.CurrentPosition;
            return vectorToDestination.sqrMagnitude < DELTA;
        }
        private Vector3 GetRandomPosition(float y)
        {
            float randomX = _random.Next(_fieldWidth);
            float randomZ = _random.Next(_fieldHeight);
            return new Vector3(randomX, y, randomZ);
        }

        #endregion


        #region IUpdatable

        public void Update(float deltaTime)
        {
            foreach (INavigation monsterNavigation in _monsterNavigations.Values)
            {
                if (IsMonsterReachdDestination(monsterNavigation))
                {
                    Vector3 newTargetPosition = GetRandomPosition(monsterNavigation.CurrentPosition.y);
                    monsterNavigation.SetDestination(newTargetPosition);
                }
            }
        }
        
        #endregion


        #region IDisposable

        public void Dispose()
        {
            _gameEventSystem.OnMonsterSpawned -= AddNewMonster;
            _gameEventSystem.OnRoundStarted -= PrepareNewRound;
            _gameEventSystem.OnMonsterDead -= RemoveMonster;

            _monsterNavigations.Clear();
        }

        #endregion

    }
}
