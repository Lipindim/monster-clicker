﻿using MonsterClicker.Enums;
using MonsterClicker.Models;
using MonsterClicker.Tools;
using System;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class HitsTheCameraController : IDisposable
    {

        #region Fields

        private readonly Camera _camera;
        private readonly GameEventSystem _gameEventSystem;

        #endregion


        #region ClassLifeCycles

        public HitsTheCameraController(GameEventSystem gameEventSystem, Camera camera)
        {
            _camera = camera;
            _gameEventSystem = gameEventSystem;
            _gameEventSystem.OnMonsterSpawned += DetectedMonsterHitInTheCamera;
        }

        #endregion


        #region Methods

        private void DetectedMonsterHitInTheCamera(MonsterModel monster)
        {
            Vector3 viewportPoint = _camera.WorldToViewportPoint(monster.CurrentPosition);
            ArrowEnum arrowEnum = GetArrow(viewportPoint.x, viewportPoint.y);
            if (arrowEnum != ArrowEnum.None)
                _gameEventSystem.SendShowArrowRequestedEvent(arrowEnum);
        }

        private ArrowEnum GetArrow(float x, float y)
        {
            if (x < 0 && y > 0 && y < 1)
                return ArrowEnum.Left;
            if (x > 1 && y > 0 && y < 1)
                return ArrowEnum.Right;
            if (x > 0 && x < 1 && y < 0)
                return ArrowEnum.Bottom;
            if (x < 0 && y < 0)
                return ArrowEnum.BottomLeft;
            if (x > 1 && y < 0)
                return ArrowEnum.BottomRight;

            return ArrowEnum.None;
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            _gameEventSystem.OnMonsterSpawned -= DetectedMonsterHitInTheCamera;
        }

        #endregion

    }
}
