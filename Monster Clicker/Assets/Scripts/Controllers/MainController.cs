﻿using MonsterClicker.Enums;
using MonsterClicker.Interfaces;
using MonsterClicker.Tools;
using System;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class MainController : IUpdatable, IDisposable
    {

        #region Fields

        private readonly GameEventSystem _gameEventSystem;
        private readonly UiController _uiController;
        private readonly GameController _gameController;

        #endregion


        #region ClassLifeCycles

        public MainController(Transform placeForUi, Transform cameraPoint)
        {
            if (placeForUi == null)
                throw new ArgumentNullException("Didn't set UI point.");
            if (cameraPoint == null)
                throw new ArgumentNullException("Didn't set camera point.");

            _gameEventSystem = new GameEventSystem();
            _uiController = new UiController(_gameEventSystem, placeForUi);
            _gameController = new GameController(_gameEventSystem, cameraPoint);
            _gameEventSystem.OnExitGameRequested += ExitGame;
            _gameEventSystem.SendShowMenuRequestedEvent(MenuTabEnum.Main);
        }

        #endregion


        #region Methods

        private void ExitGame()
        {
            Application.Quit();
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            _gameEventSystem.OnExitGameRequested -= ExitGame;

            _uiController.Dispose();
            _gameController.Dispose();
        }

        #endregion


        #region IUpdatable

        public void Update(float deltaTime)
        {
            _gameController.Update(deltaTime);
            _uiController.Update(deltaTime);
        }

        #endregion

    }
}
