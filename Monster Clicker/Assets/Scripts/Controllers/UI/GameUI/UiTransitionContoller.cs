﻿using MonsterClicker.Enums;
using MonsterClicker.Settings;
using MonsterClicker.Tools;
using System;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class UiTransitionContoller : IDisposable
    {
        #region Constants

        private const string VIEW_PATH_RESTART = "Prefabs/Restart";
        private const string VIEW_PATH_ROUND_COMPLETED = "Prefabs/RoundCompleted";
        private const string VIEW_PATH_GAME_COMLETED = "Prefabs/GameCompleted";
        private const string VIEW_SAVE_SCORE = "Prefabs/SaveScore";

        #endregion


        #region Fields

        private readonly UiSimpleObjectController _restartController;
        private readonly UiSimpleObjectController _roundCompletedController;
        private readonly UiSimpleObjectController _gameCompletedController;
        private readonly SaveScoreController _saveScoreController;
        private readonly DisplayScoreController _displayScoreController;
        private readonly GameEventSystem _gameEventSystem;

        #endregion


        #region ClassLifeCycles

        public UiTransitionContoller(GameEventSystem gameEventSystem, Transform placeForUi, DisplayScoreController displayScoreController)
        {
            var recordsStore = new RecordsStore();
            _restartController = new UiSimpleObjectController(gameEventSystem, placeForUi, VIEW_PATH_RESTART);
            _roundCompletedController = new UiSimpleObjectController(gameEventSystem, placeForUi, VIEW_PATH_ROUND_COMPLETED);
            _gameCompletedController = new UiSimpleObjectController(gameEventSystem, placeForUi, VIEW_PATH_GAME_COMLETED);
            _saveScoreController = new SaveScoreController(gameEventSystem, placeForUi, VIEW_SAVE_SCORE, recordsStore);
            _displayScoreController = displayScoreController;

            _gameEventSystem = gameEventSystem;

            _gameEventSystem.OnShowMenuRequested += HideUiTransitions;
            _gameEventSystem.OnRoundStarted += HideUiTransitions;
            _gameEventSystem.OnGameOver += ShowRestart;
            _gameEventSystem.OnRoundCompleted += ShowRoundCompleted;
            _gameEventSystem.OnGameCompleted += ShowGameCompleted;
        }

        #endregion


        #region Methods

        private void ShowScore(UiSimpleObjectController nextController)
        {
            if (_displayScoreController.CurrentIsRecord())
            {
                int score = _displayScoreController.CurrentScore;
                _saveScoreController.SetScore(score);
                _saveScoreController.SetNextAction(() =>
                {
                    _saveScoreController.Hide();
                    nextController.Show();
                });
                _displayScoreController.ResetScore();
                _saveScoreController.Show();
            }
            else
            {
                nextController.Show();
            }
        }

        private void ShowGameCompleted()
        {
            ShowScore(_gameCompletedController);
        }

        private void ShowRoundCompleted()
        {
            _roundCompletedController.Show();
        }

        private void ShowRestart()
        {
            ShowScore(_restartController);
        }

        private void HideUiTransitions(RoundSettings roundSettings)
        {
            HideAll();
        }

        private void HideUiTransitions(MenuTabEnum menuTab)
        {
            HideAll();
        }

        private void HideAll()
        {
            _gameCompletedController.Hide();
            _restartController.Hide();
            _roundCompletedController.Hide();
            _saveScoreController.Hide();
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            _gameEventSystem.OnShowMenuRequested -= HideUiTransitions;
            _gameEventSystem.OnRoundStarted -= HideUiTransitions;
            _gameEventSystem.OnGameOver -= ShowRestart;
            _gameEventSystem.OnRoundCompleted -= ShowRoundCompleted;
            _gameEventSystem.OnGameCompleted -= ShowGameCompleted;

            _restartController.Dispose();
            _roundCompletedController.Dispose();
            _gameCompletedController.Dispose();
            _saveScoreController.Dispose();
        }

        #endregion

    }
}
