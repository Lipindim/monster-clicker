﻿using MonsterClicker.Models;
using MonsterClicker.Tools;
using MonsterClicker.Views;
using System;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class MonsterCountController : UiObjectController<MonsterCountView>
    {

        #region Fields

        private int _currentMonsterCount;

        #endregion


        #region ClassLifeCycles

        public MonsterCountController(GameEventSystem gameEventSystem, Transform placeForUi, string viewPath) : base(gameEventSystem, placeForUi, viewPath)
        {
            _gameEventSystem.OnMonsterSpawned += IncreaseMonsterCount;
            _gameEventSystem.OnMonsterDead += DecreaseMonsterCount;
            _gameEventSystem.OnRoundStarted += Reset;
        }

        #endregion


        #region Methods

        private void Reset(Settings.RoundSettings obj)
        {
            _currentMonsterCount = 0;
            ShowMonsterCount();
        }

        private void DecreaseMonsterCount(Guid id)
        {
            _currentMonsterCount--;
            ShowMonsterCount();
        }

        private void IncreaseMonsterCount(MonsterModel monster)
        {
            _currentMonsterCount++;
            ShowMonsterCount();
        }

        private void ShowMonsterCount()
        {
            if (!_uiObjectCreated)
                Show();
            if (_view == null)
                _view = _uiObject.GetComponent<MonsterCountView>();

            _view.SetMonsterCount(_currentMonsterCount);
        }

        protected override void InitializeView()
        {
            _view.Initialize(_gameEventSystem);
        }

        #endregion


        #region IDisposable

        protected override void OnDispose()
        {

            _gameEventSystem.OnMonsterSpawned -= IncreaseMonsterCount;
            _gameEventSystem.OnMonsterDead -= DecreaseMonsterCount;
            _gameEventSystem.OnRoundStarted -= Reset;
        }

        #endregion

    }
}
