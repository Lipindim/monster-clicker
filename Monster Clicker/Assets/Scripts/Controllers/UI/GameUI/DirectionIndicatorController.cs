﻿using MonsterClicker.Enums;
using MonsterClicker.Interfaces;
using MonsterClicker.Tools;
using MonsterClicker.Views;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class DirectionIndicatorController : UiObjectController<DirectionIndicatorView>, IUpdatable
    {

        #region Constants

        private const float SHOW_TIME = 1.0f;

        #endregion


        #region Fields

        private readonly Dictionary<ArrowEnum, float> _arrows;

        #endregion


        #region ClassLifeCycles

        public DirectionIndicatorController(GameEventSystem gameEventSystem, Transform placeForUi, string viewPath) : base(gameEventSystem, placeForUi, viewPath)
        {
            _arrows = new Dictionary<ArrowEnum, float>();

            _gameEventSystem.OnShowArrowRequested += ShowArrow;
        }

        #endregion


        #region Methods

        private void ShowArrow(ArrowEnum arrowEnum)
        {
            _arrows[arrowEnum] = SHOW_TIME;
            _view.ShowArrow(arrowEnum);
        }
        protected override void InitializeView()
        {
            _view.Initialize(_gameEventSystem);
        }

        protected override void OnDispose()
        {
            _gameEventSystem.OnShowArrowRequested -= ShowArrow;
        }

        #endregion


        #region IUpdatable

        public void Update(float deltaTime)
        {
            IEnumerable<ArrowEnum> keys = _arrows.Keys.ToArray();
            foreach (var key in keys)
            {
                if (_arrows[key] > 0)
                {
                    _arrows[key] -= deltaTime;

                    if (_arrows[key] <= 0)
                        _view.HideArrow(key);
                }
            }
        }

        #endregion

    }
}
