﻿using MonsterClicker.Models;
using MonsterClicker.Tools;
using MonsterClicker.Views;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class DisplayScoreController : UiObjectController<ScoreView>
    {

        #region Fields

        private readonly RecordsStore _recordStore;
        private readonly Dictionary<Guid, int> _monstersScore;

        private int _currentScore;

        #endregion


        #region Properties

        public int CurrentScore => _currentScore;

        #endregion


        #region ClassLifeCycles

        public DisplayScoreController(GameEventSystem gameEventSystem, Transform placeForUi, string viewPath, RecordsStore recordsStore) : base(gameEventSystem, placeForUi, viewPath)
        {
            _recordStore = recordsStore;
            _monstersScore = new Dictionary<Guid, int>();
            _gameEventSystem.OnMonsterSpawned += RememberScore;
            _gameEventSystem.OnMonsterDead += IncreaseScore;
        }

        #endregion


        #region Methods

        public void ResetScore()
        {
            _currentScore = 0;
            _view.SetScore(_currentScore);
        }
        public bool CurrentIsRecord()
        {
            return _recordStore.IsRecord(_currentScore);
        }

        private void RememberScore(MonsterModel monster)
        {
            _monstersScore.Add(monster.Id, monster.Score);
        }

        private void IncreaseScore(Guid id)
        {
            if (_monstersScore.ContainsKey(id))
            {
                _currentScore += _monstersScore[id];
                _monstersScore.Remove(id);
                _view.SetScore(_currentScore);
            }    
        }

        protected override void InitializeView()
        {
            _view.Initialize(_gameEventSystem);
        }

        
        protected override void OnDispose()
        {
            _gameEventSystem.OnMonsterSpawned -= RememberScore;
            _gameEventSystem.OnMonsterDead -= IncreaseScore;
            _gameEventSystem.OnGameOver -= ResetScore;
        }

        #endregion

    }
}
