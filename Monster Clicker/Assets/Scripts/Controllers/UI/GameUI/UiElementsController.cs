﻿using MonsterClicker.Enums;
using MonsterClicker.Interfaces;
using MonsterClicker.Settings;
using MonsterClicker.Tools;
using System;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class UiElementsController : IDisposable, IUpdatable
    {

        #region Constants

        private const string VIEW_DIRECTION_INDICATOR = "Prefabs/DirectionIndicator";
        private const string VIEW_MONSTER_COUNT = "Prefabs/MonsterCount";
        private const string VIEW_SCORE = "Prefabs/Score";

        #endregion


        #region Fields

        private readonly DirectionIndicatorController _directionIndicatorController;
        private readonly MonsterCountController _monsterCountController;
        private readonly DisplayScoreController _displayScoreController;
        private readonly GameEventSystem _gameEventSystem;

        #endregion


        #region Properties

        public DisplayScoreController DisplayScoreController => _displayScoreController;

        #endregion


        #region ClassLifeCycles

        public UiElementsController(GameEventSystem gameEventSystem, Transform placeForUi, RecordsStore recordsStore)
        {
            _directionIndicatorController = new DirectionIndicatorController(gameEventSystem, placeForUi, VIEW_DIRECTION_INDICATOR);
            _monsterCountController = new MonsterCountController(gameEventSystem, placeForUi, VIEW_MONSTER_COUNT);
            _displayScoreController = new DisplayScoreController(gameEventSystem, placeForUi, VIEW_SCORE, recordsStore);

            _gameEventSystem = gameEventSystem;

            _gameEventSystem.OnShowMenuRequested += HideUiElements;
            _gameEventSystem.OnRoundStarted += ShowUiElements;
        }

        private void ShowUiElements()
        {
            throw new NotImplementedException();
        }

        #endregion


        #region Methods

        private void ShowUiElements(RoundSettings roundSettings)
        {
            _monsterCountController.Show();
            _directionIndicatorController.Show();
            _displayScoreController.Show();
        }

        private void HideUiElements(MenuTabEnum menuTab)
        {
            _monsterCountController.Hide();
            _directionIndicatorController.Hide();
            _displayScoreController.Hide();
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            _gameEventSystem.OnShowMenuRequested -= HideUiElements;
            _gameEventSystem.OnRoundStarted -= ShowUiElements;

            _directionIndicatorController.Dispose();
            _monsterCountController.Dispose();
            _displayScoreController.Dispose();
        }

        #endregion


        #region IUpdatable

        public void Update(float deltaTime)
        {
            _directionIndicatorController.Update(deltaTime);
        }

        #endregion

    }
}
