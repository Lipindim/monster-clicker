﻿using MonsterClicker.Tools;
using MonsterClicker.Views;
using System;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class SaveScoreController : UiObjectController<SaveScoreView>
    {

        #region Fields

        private readonly RecordsStore _recordsStore;

        private Action _nextAction;

        private int _score;

        #endregion


        #region ClassLifeCycles

        public SaveScoreController(GameEventSystem gameEventSystem, Transform placeForUi, string viewPath, RecordsStore recordsStore) : base(gameEventSystem, placeForUi, viewPath)
        {
            _recordsStore = recordsStore;
        }

        #endregion


        #region Methods

        public override void Show()
        {
            base.Show();
            _view.DisplayScore(_score);
        }

        public void SetScore(int score)
        {
            _score = score;
            
        }

        public void SetNextAction(Action nextAction)
        {
            _nextAction = nextAction;
        }

        protected override void InitializeView()
        {
            _view.Initialize(SaveScore);
        }

        private void SaveScore()
        {
            _recordsStore.SaveRecord(_score, _view.Name);
            _nextAction();
        }

        #endregion

    }
}
