﻿using MonsterClicker.Interfaces;
using MonsterClicker.Tools;
using System;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class UiController : IDisposable, IUpdatable
    {

        #region Fields

        private readonly MainMenuController _mainMenuController;
        private readonly UiElementsController _uiElementsController;
        private readonly UiTransitionContoller _uiTransitionController;

        #endregion


        #region ClassLifeCycles

        public UiController(GameEventSystem gameEventSystem, Transform placeForUi)
        {
            var recordStore = new RecordsStore();
            _mainMenuController = new MainMenuController(gameEventSystem, placeForUi, recordStore);
            _uiElementsController = new UiElementsController(gameEventSystem, placeForUi, recordStore);
            _uiTransitionController = new UiTransitionContoller(gameEventSystem, placeForUi, _uiElementsController.DisplayScoreController);
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            _mainMenuController.Dispose();
            _uiElementsController.Dispose();
            _uiTransitionController.Dispose();
        }

        #endregion


        #region IUpdatable

        public void Update(float deltaTime)
        {
            _uiElementsController.Update(deltaTime);
        }

        #endregion

    }
}
