﻿using MonsterClicker.Enums;
using MonsterClicker.Settings;
using MonsterClicker.Tools;
using System;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class MainMenuController : IDisposable
    {
        #region Constants

        private const string VIEW_PATH_MAIN = "Prefabs/MainMenu";
        private const string VIEW_PATH_RECORDS = "Prefabs/Records";
        private const string VIEW_PATH_CREDITS = "Prefabs/Credits";

        #endregion


        #region Fields

        private readonly UiSimpleObjectController _mainMenuTabController;
        private readonly RecordsController _recordsMenuTabController;
        private readonly UiSimpleObjectController _creditsMenuTabController;
        private readonly GameEventSystem _gameEventSystem;

        #endregion


        #region ClassLifeCycles

        public MainMenuController(GameEventSystem gameEventSystem, Transform placeForUi, RecordsStore recordsStore)
        {
            _mainMenuTabController = new UiSimpleObjectController(gameEventSystem, placeForUi, VIEW_PATH_MAIN);
            _recordsMenuTabController = new RecordsController(gameEventSystem, placeForUi, VIEW_PATH_RECORDS, recordsStore);
            _creditsMenuTabController = new UiSimpleObjectController(gameEventSystem, placeForUi, VIEW_PATH_CREDITS);
          

            _gameEventSystem = gameEventSystem;

            _gameEventSystem.OnShowMenuRequested += ShowMenu;
            _gameEventSystem.OnRoundStarted += PrepareStartGame;
        }

        #endregion


        #region Methods


        private void PrepareStartGame(RoundSettings roundSettings)
        {
            HideAllTabs();
        }

        private void ShowMenu(MenuTabEnum menuTab)
        {
            HideAllTabs();
            switch (menuTab)
            {
                case MenuTabEnum.Main:
                    {
                        _mainMenuTabController.Show();
                        break;
                    }
                case MenuTabEnum.Records:
                    {
                        _recordsMenuTabController.Show();
                        break;
                    }
                case MenuTabEnum.Credits:
                    {
                        _creditsMenuTabController.Show();
                        break;
                    }
                default:
                    throw new ArgumentException($"Unknown menu tab: {menuTab}");
            }
        }

        private void HideAllTabs()
        {
            _mainMenuTabController.Hide();
            _recordsMenuTabController.Hide();
            _creditsMenuTabController.Hide();
        }

        #endregion


        #region IDisposable

        public void Dispose()
        {
            _gameEventSystem.OnShowMenuRequested -= ShowMenu;
            _gameEventSystem.OnRoundStarted -= PrepareStartGame;

            _mainMenuTabController.Dispose();
            _recordsMenuTabController.Dispose();
            _creditsMenuTabController.Dispose();
        }

        #endregion
    }
}
