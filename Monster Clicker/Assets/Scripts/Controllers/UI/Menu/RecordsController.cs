﻿using MonsterClicker.Models;
using MonsterClicker.Tools;
using MonsterClicker.Views;
using System.Collections.Generic;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class RecordsController : UiObjectController<RecordsView>
    {
        private readonly RecordsStore _recordsStore;


        public RecordsController(GameEventSystem gameEventSystem, Transform placeForUi, string viewPath, RecordsStore recordsStore) : base(gameEventSystem, placeForUi, viewPath)
        {
            _recordsStore = recordsStore;
        }

        public override void Show()
        {
            base.Show();

            List<RecordModel> records = _recordsStore.GetRecords();
            _view.ShowRecords(records);
        }

        protected override void InitializeView()
        {
            _view.Initialize(_gameEventSystem);
        }
    }
}
