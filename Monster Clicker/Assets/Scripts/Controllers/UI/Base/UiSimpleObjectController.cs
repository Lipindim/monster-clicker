﻿using MonsterClicker.Tools;
using MonsterClicker.Views;
using UnityEngine;


namespace MonsterClicker.Controllers
{
    public class UiSimpleObjectController : UiObjectController<UiObjectView>
    {
        public UiSimpleObjectController(GameEventSystem gameEventSystem, Transform placeForUi, string viewPath) 
            : base(gameEventSystem, placeForUi, viewPath)
        {
        }

        protected override void InitializeView()
        {
            _view.Initialize(_gameEventSystem);
        }

    }
}

