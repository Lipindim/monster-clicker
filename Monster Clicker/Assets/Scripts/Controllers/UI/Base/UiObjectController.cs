﻿using MonsterClicker.Tools;
using System;
using UnityEngine;

namespace MonsterClicker.Controllers
{
    public class UiObjectController<TView> : IDisposable
    {

        #region Fields

        protected readonly Transform _placeForUi;
        protected readonly GameEventSystem _gameEventSystem;
        protected readonly string _viewPath;

        protected GameObject _uiObject;
        protected TView _view;

        protected bool _uiObjectCreated;

        #endregion


        #region ClassLifeCycles

        public UiObjectController(GameEventSystem gameEventSystem, Transform placeForUi, string viewPath)
        {
            _placeForUi = placeForUi;
            _gameEventSystem = gameEventSystem;
            _viewPath = viewPath;
        }

        #endregion


        #region Methods

        public virtual void Show()
        {
            if (_uiObjectCreated)
            {
                _uiObject.SetActive(true);
            }
            else
            {
                _view = LoadView();
                InitializeView();
                _uiObjectCreated = true;
            }
        }

        public void Hide()
        {
            if (_uiObjectCreated)
                _uiObject.SetActive(false);
        }

        protected virtual void InitializeView()
        {

        }

        private TView LoadView()
        {
            GameObject prefab = Resources.Load<GameObject>(_viewPath);
            _uiObject = GameObject.Instantiate(prefab, _placeForUi);
            return _uiObject.GetComponent<TView>();
        }

        #endregion


        #region IDispasable

        public void Dispose()
        {
            if (_uiObjectCreated)
                GameObject.Destroy(_uiObject);

            OnDispose();
        }

        protected virtual void OnDispose()
        {

        }

        #endregion

    }
}
